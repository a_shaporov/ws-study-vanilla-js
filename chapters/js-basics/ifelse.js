'use strict'
/*
Используя конструкцию if..else, напишите код, который будет спрашивать: „Какое «официальное» название JavaScript?“

Если пользователь вводит «ECMAScript», то показать: «Верно!», в противном случае – отобразить: «Не знаете? ECMAScript!»
 */
{
    const answer = prompt("Какое «официальное» название JavaScript?", "ECMAScript");

    if (answer === "ECMAScript") {
        alert("Верно!");
    } else {
        alert("Не знаете? ECMAScript!");
    }
}


/*
Используя конструкцию if..else, напишите код, который получает число через prompt, а затем выводит в alert:

1, если значение больше нуля,
-1, если значение меньше нуля,
0, если значение равно нулю.
Предполагается, что пользователь вводит только числа.
 */

{
    const answer = +prompt("Введите число", "1");

    if (answer > 0) {
        alert(1);
    } else if (answer === 0) {
        alert(0);
    } else {
        alert(-1);
    }
}

/*
Перепишите конструкцию if с использованием условного оператора '?':

let result;

if (a + b < 4) {
  result = 'Мало';
} else {
  result = 'Много';
}
 */

{
    let result, a, b;

    a + b < 4 ? result = "мало" : result = "много";
}

/*
Перепишите if..else с использованием нескольких операторов '?'.

Для читаемости рекомендуется разбить код на несколько строк.

let message;

if (login == 'Сотрудник') {
  message = 'Привет';
} else if (login == 'Директор') {
  message = 'Здравствуйте';
} else if (login == '') {
  message = 'Нет логина';
} else {
  message = '';
}
 */

{
    let message, login;

    login == "Сотрудник" ? message = "Привет" :
    login == "Директор" ? message = "Здравствуйте" :
    login == "" ? message = "Нет логина" : message = "";
}