/*
Допустим, у нас есть массив arr.

Создайте функцию unique(arr), которая вернёт массив уникальных, не повторяющихся значений массива arr.

P.S. Здесь мы используем строки, но значения могут быть любого типа.

    P.P.S. Используйте Set для хранения уникальных значений.
 */

function unique(arr) {
  return Array.from(
    new Set(arr) //как раз из прошлого задания
  );
}

let values = [
  "Hare",
  "Krishna",
  "Hare",
  "Krishna",
  "Krishna",
  "Krishna",
  "Hare",
  "Hare",
  ":-O",
];

alert(unique(values)); // Hare,Krishna,:-O

/*
Анаграммы – это слова, у которых те же буквы в том же количестве, но они располагаются в другом порядке.

Например:

nap - pan
ear - are - era
cheaters - hectares - teachers
Напишите функцию aclean(arr), которая возвращает массив слов, очищенный от анаграмм.

Из каждой группы анаграмм должно остаться только одно слово, не важно какое.
 */

let arr = ["nap", "teachers", "cheaters", "PAN", "ear", "era", "hectares"];

alert(aclean(arr)); // "nap,teachers,ear" или "PAN,cheaters,era"

function aclean(inputArray) {
  const map = new Map();

  inputArray.forEach((inputWord) => {
    let transformedWord = inputWord //PAN
      .toLowerCase() //pan
      .split("") //p a n
      .sort() //a n p
      .join(""); //anp

    map.set(transformedWord, inputWord)
  });

  return Array.from(map.values());
}

/*
Мы хотели бы получить массив ключей map.keys() в переменную и далее работать с ними, например, применить метод .push.

Но это не выходит:

let map = new Map();

map.set("name", "John");

let keys = map.keys();

// Error: keys.push is not a function
// Ошибка: keys.push -- это не функция
keys.push("more");

Почему? Что нужно поправить в коде, чтобы вызов keys.push сработал?

 */

let map = new Map();

map.set("name", "John");

let keys = Array.from(map.keys()); //добавляем Array.from

keys.push("more");
