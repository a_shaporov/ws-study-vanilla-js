/*
Создайте скрипт, который запрашивает ввод двух чисел (используйте prompt) и после показывает их сумму.
 */
let a = parseInt( prompt('enter value 1', '') ),
    b = parseInt( prompt('enter value 2', '') );

function sum(a, b) {
    if (isNaN(a) || isNaN(b)) {
        return 0;
    }

    return a + b;
}

alert( sum(a, b) );

/*
Создайте функцию readNumber,
которая будет запрашивать ввод числового значения до тех пор, пока посетитель его не введёт.

Функция должна возвращать числовое значение.

Также надо разрешить пользователю остановить процесс ввода,
отправив пустую строку или нажав «Отмена». В этом случае функция должна вернуть null.
 */

function readNumber() {
    let read = prompt('enter value', '');

    if (read === "" || read === null) return null;

    if ( isNaN(+read) ) {
        readNumber();
    } else return read;

}

readNumber();

/*
Встроенный метод Math.random() возвращает случайное число от 0 (включительно) до 1 (но не включая 1)

Напишите функцию random(min, max), которая генерирует случайное число с плавающей точкой от min до max (но не включая max).

Пример работы функции:

alert( random(1, 5) ); // 1.2345623452
alert( random(1, 5) ); // 3.7894332423
alert( random(1, 5) ); // 4.3435234525
 */

function random(min = 0, max = 1) {
    let difference = max - min;
    return Math.random() * difference + min;
}

/*
Напишите функцию randomInteger(min, max),
которая генерирует случайное целое (integer) число от min до max (включительно).

Любое число из интервала min..max должно появляться с одинаковой вероятностью.

Пример работы функции:

alert( randomInteger(1, 5) ); // 1
alert( randomInteger(1, 5) ); // 3
alert( randomInteger(1, 5) ); // 5
 */

function randomInteger(min = 0, max = 1) {
    return ( Math.floor( random(min, max + 1) ) );
}

console.log( randomInteger(1, 5) );
