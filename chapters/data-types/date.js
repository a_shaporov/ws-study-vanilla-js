/*
Создайте объект Date для даты: 20 февраля 2012 года, 3 часа 12 минут. Временная зона – местная.

Для вывода используйте alert.
 */

alert(new Date(2012, 1, 20, 3, 12));

/*
Напишите функцию getWeekDay(date), показывающую день недели в коротком формате: «ПН», «ВТ», «СР», «ЧТ», «ПТ», «СБ», «ВС».

Например:
 
let date = new Date(2012, 0, 3);  // 3 января 2012 года
alert( getWeekDay(date) );        // нужно вывести "ВТ"
 */

function getWeekDay(date) {
  switch (date.getDay()) {
    case 0:
      return "ВС";
    case 1:
      return "ПН";
    case 2:
      return "ВТ";
    case 3:
      return "СР";
    case 4:
      return "ЧТ";
    case 5:
      return "ПТ";
    case 6:
      return "СБ";
  }
}
let date = new Date(2012, 0, 3);
alert(getWeekDay(date));

/*
В Европейских странах неделя начинается с понедельника (день номер 1),
затем идёт вторник (номер 2) и так до воскресенья (номер 7).
Напишите функцию getLocalDay(date), которая возвращает «европейский» день недели для даты date.

let date = new Date(2012, 0, 3);  // 3 января 2012 года
alert( getLocalDay(date) );       // вторник, нужно показать 2
 */

let date = new Date(2012, 0, 3); // 3 января 2012 года
alert(getLocalDay(date)); // вторник, нужно показать 2

let date = new Date(2012, 0, 8); // 8 января 2012 года
alert(getLocalDay(date)); // воскресенье, нужно показать 7

function getLocalDay(date) {
  return date.getDay() || 7;
}

/*
Создайте функцию getDateAgo(date, days), возвращающую число, которое было days дней назад от даты date.

К примеру, если сегодня двадцатое число, то getDateAgo(new Date(), 1) вернёт девятнадцатое и getDateAgo(new Date(), 2) – восемнадцатое.

Функция должна надёжно работать при значении days=365 и больших значениях:

let date = new Date(2015, 0, 2);

alert( getDateAgo(date, 1) ); // 1, (1 Jan 2015)
alert( getDateAgo(date, 2) ); // 31, (31 Dec 2014)
alert( getDateAgo(date, 365) ); // 2, (2 Jan 2014)
P.S. Функция не должна изменять переданный ей объект date.
 */

let date = new Date(2015, 0, 2);

alert(getDateAgo(date, 1)); // 1, (1 Jan 2015)
alert(getDateAgo(date, 2)); // 31, (31 Dec 2014)
alert(getDateAgo(date, 365)); // 2, (2 Jan 2014)

function getDateAgo(date, days) {
  return new Date(date.getTime() - days * 86400000).getDate();
}

/*
Напишите функцию getLastDayOfMonth(year, month),
возвращающую последнее число месяца. Иногда это 30, 31 или даже февральские 28/29.

Параметры:

year – год из четырёх цифр, например, 2012.
month – месяц от 0 до 11.
К примеру, getLastDayOfMonth(2012, 1) = 29 (високосный год, февраль).
 */

console.log(getLastDayOfMonth(2012, 1)); // = 29 (високосный год, февраль).

function getLastDayOfMonth(year, month) {
  return new Date(year, month + 1, 0).getDate();
}

/*
Напишите функцию getSecondsToday(),
возвращающую количество секунд с начала сегодняшнего дня.

Например, если сейчас 10:00, и не было перехода на зимнее/летнее время, то:

getSecondsToday() == 36000 // (3600 * 10)
Функция должна работать в любой день, т.е. в ней не должно быть конкретного значения сегодняшней даты.
 */

console.log(getSecondsToday());

function getSecondsToday() {
  const t = new Date();
  return t.getSeconds() + t.getMinutes() * 60 + t.getHours() * 3600;
}

/*
Создайте функцию getSecondsToTomorrow(), возвращающую количество секунд до завтрашней даты.

Например, если сейчас 23:00, то:

getSecondsToTomorrow() == 3600
P.S. Функция должна работать в любой день, т.е. в ней не должно быть конкретного значения сегодняшней даты.
 */

console.log(getSecondsToTomorrow());

function getSecondsToTomorrow() {
  return 86400 - getSecondsToday();
}

/*
Напишите функцию formatDate(date), форматирующую date по следующему принципу:

Если спустя date прошло менее 1 секунды, вывести "прямо сейчас".
В противном случае, если с date прошло меньше 1 минуты, вывести "n сек. назад".
В противном случае, если меньше часа, вывести "m мин. назад".
В противном случае, полная дата в формате "DD.MM.YY HH:mm". А именно: "день.месяц.год часы:минуты", всё в виде двух цифр, т.е. 31.12.16 10:00.
 */

alert(formatDate(new Date(new Date() - 1))); // "прямо сейчас"

alert(formatDate(new Date(new Date() - 30 * 1000))); // "30 сек. назад"

alert(formatDate(new Date(new Date() - 5 * 60 * 1000))); // "5 мин. назад"

// вчерашняя дата вроде 31.12.2016, 20:00
alert(formatDate(new Date(new Date() - 86400 * 1000)));

function formatDate(date) {
  const difference = new Date(Date.now() - date.getTime()),
    stamp = difference.getTime();

  if (stamp > 3600000) {
    return describeFullDate(date);
  } else if (stamp > 60000) {
    return `${difference.getMinutes()} мин. назад`;
  } else if (stamp > 1000) {
    return `${difference.getSeconds()} сек. назад`;
  } else {
    return "прямо сейчас";
  }
}

function describeFullDate(date) {
  let day = date.getDate(),
    month = date.getMonth() + 1,
    year = `${date.getFullYear()}`.slice(2),
    hours = date.getHours(),
    minutes = date.getMinutes();

  if (day < 10) day = `0${day}`;
  if (month < 10) month = `0${month}`;
  if (minutes < 10) minutes = `0${minutes}`;
  if (hours < 10) hours = `0${hours}`;

  return `${day}.${month}.${year} ${hours}:${minutes}`;
}
