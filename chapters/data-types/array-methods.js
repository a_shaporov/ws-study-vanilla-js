/*
Напишите функцию camelize(str),
которая преобразует строки вида «my-short-string» в «myShortString».

То есть дефисы удаляются, а все слова после них получают заглавную букву.

Примеры:

camelize("background-color") == 'backgroundColor';
camelize("list-style-image") == 'listStyleImage';
camelize("-webkit-transition") == 'WebkitTransition';
 */

function camelize(str) {
  return str
    .split("-")
    .map((word, index) =>
      index == 0 ? word : word[0].toUpperCase() + word.slice(1)
    )
    .join("");
}

console.log(
  camelize("background-color"),
  camelize("list-style-image"),
  camelize("-webkit-transition")
);

/*
Напишите функцию filterRange(arr, a, b), которая принимает массив arr, ищет в нём элементы между a и b и отдаёт массив этих элементов.

Функция должна возвращать новый массив и не изменять исходный.

Например:

let arr = [5, 3, 8, 1];

let filtered = filterRange(arr, 1, 4);

alert( filtered ); // 3,1 (совпадающие значения)

alert( arr ); // 5,3,8,1 (без изменений)
 */

function filterRange(arr, a, b) {
  const max = Math.max(a, b);
  const min = Math.min(a, b);

  return arr.filter((value) => {
    if (min <= value && value <= max) return true;
  });
}

/*
Напишите функцию filterRangeInPlace(arr, a, b),
которая принимает массив arr и удаляет из него все значения кроме тех,
которые находятся между a и b. То есть, проверка имеет вид a ≤ arr[i] ≤ b.

Функция должна изменять принимаемый массив и ничего не возвращать.

Например:

let arr = [5, 3, 8, 1];

filterRangeInPlace(arr, 1, 4); // удалены числа вне диапазона 1..4

alert( arr ); // [3, 1]
 */

function filterRangeInPlace(arr, a, b) {
  let max = Math.max(a, b);
  let min = Math.min(a, b);

  for (let i = 0; i < arr.length; i++) {
    if (arr[i] < min || arr[i] > max) {
      arr.splice(i, 1);
      i--;
    }
  }
}

/*
Сортировать в порядке по убыванию

let arr = [5, 2, 1, -10, 8];

// ... ваш код для сортировки по убыванию

alert( arr ); // 8, 5, 2, 1, -10
 */

let arr = [5, 2, 1, -10, 8];

arr.sort((a, b) => {
  return b - a;
});

alert(arr); // 8, 5, 2, 1, -10

/*
У нас есть массив строк arr. Нужно получить отсортированную копию, но оставить arr неизменённым.

Создайте функцию copySorted(arr), которая будет возвращать такую копию.

let arr = ["HTML", "JavaScript", "CSS"];

let sorted = copySorted(arr);

alert( sorted ); // CSS, HTML, JavaScript
alert( arr ); // HTML, JavaScript, CSS (без изменений)
 */

let arr = ["HTML", "JavaScript", "CSS"];

let sorted = copySorted(arr);

alert(sorted); // CSS, HTML, JavaScript
alert(arr); // HTML, JavaScript, CSS (без изменений)

function copySorted(arr) {
  return arr.concat().sort();
}

/*
Создать расширяемый калькулятор

Создайте функцию конструктор Calculator, которая создаёт «расширяемые» объекты калькулятора.

Задание состоит из двух частей.

Во-первых, реализуйте метод calculate(str), который принимает строку типа "1 + 2" в формате «ЧИСЛО оператор ЧИСЛО» (разделено пробелами) и возвращает результат. Метод должен понимать плюс + и минус -.

Пример использования:

let calc = new Calculator;

alert( calc.calculate("3 + 7") ); // 10
Затем добавьте метод addMethod(name, func), который добавляет в калькулятор новые операции. Он принимает оператор name и функцию с двумя аргументами func(a,b), которая описывает его.

Например, давайте добавим умножение *, деление / и возведение в степень **:

let powerCalc = new Calculator;
powerCalc.addMethod("*", (a, b) => a * b);
powerCalc.addMethod("/", (a, b) => a / b);
powerCalc.addMethod("**", (a, b) => a ** b);

let result = powerCalc.calculate("2 ** 3");
alert( result ); // 8
Для этой задачи не нужны скобки или сложные выражения.
Числа и оператор разделены ровно одним пробелом.
Не лишним будет добавить обработку ошибок.
 */

let calc = new Calculator();

alert(calc.calculate("3 + 7"));

let powerCalc = new Calculator();
powerCalc.addMethod("*", (a, b) => a * b);
powerCalc.addMethod("/", (a, b) => a / b);
powerCalc.addMethod("**", (a, b) => a ** b);

let result = powerCalc.calculate("2 ** 3");
alert(result);

function Calculator() {
  this.methods = [
    // <= Массив, лол
    "+",
    (a, b) => a + b,
    "-",
    (a, b) => a - b,
  ];

  /*
     Примечание разработчика:

     Я решил реализовать методы как массив, руководствуясь тем, что тема урока - методы массива.
     Тем не менее, на сайте в ответах, как оказалось, методы - это просто ключи и свойства внутреннего объекта
     Лол

     this.methods = {
     "-": (a, b) => a - b,
     "+": (a, b) => a + b
     };

     */

  this.calculate = (string) => {
    const calcExpression = string.split(" ");

    const argA = +calcExpression[0],
      calcOperator = calcExpression[1],
      argB = +calcExpression[2];

    const operatorIndex = this.methods.indexOf(calcOperator),
      methodIndex = operatorIndex + 1;

    return this.methods[methodIndex](argA, argB);
  };
  this.addMethod = (name, func) => {
    this.methods.push(name, func);
  };
}

/*
У вас есть массив объектов user, и в каждом из них есть user.name. Напишите код, который преобразует их в массив имён.

Например:
 */

let vasya = { name: "Вася", age: 25 };
let petya = { name: "Петя", age: 30 };
let masha = { name: "Маша", age: 28 };

let users = [vasya, petya, masha];

let names = users.map((user) => user.name); //делаем мап с исходного массива, и вытаскиваем нэймы

alert(names); // Вася, Петя, Маша

/*
У вас есть массив объектов user, и у каждого из объектов есть name, surname и id.

Напишите код, который создаст ещё один массив объектов с параметрами id и fullName, где fullName – состоит из name и surname.

*/

let vasya = { name: "Вася", surname: "Пупкин", id: 1 };
let petya = { name: "Петя", surname: "Иванов", id: 2 };
let masha = { name: "Маша", surname: "Петрова", id: 3 };

let users = [vasya, petya, masha];

let usersMapped = users.map((item) => {
  return {
    id: item.id,
    fullName: item.name + " " + item.surname,
  };
});

/*
usersMapped = [
  { fullName: "Вася Пупкин", id: 1 },
  { fullName: "Петя Иванов", id: 2 },
  { fullName: "Маша Петрова", id: 3 }
]
 */

alert(usersMapped[0].id); // 1
alert(usersMapped[0].fullName); // Вася Пупкин

/*
Напишите функцию sortByAge(users), которая принимает массив объектов со свойством age и сортирует их по нему.

 */

let vasya = { name: "Вася", age: 25 };
let petya = { name: "Петя", age: 30 };
let masha = { name: "Маша", age: 28 };

let arr = [vasya, petya, masha];

sortByAge(arr);

// теперь: [vasya, masha, petya]
alert(arr[0].name); // Вася
alert(arr[1].name); // Маша
alert(arr[2].name); // Петя

function sortByAge(array) {
  array.sort((first, second) => first.age - second.age);
}

/*
Напишите функцию shuffle(array), которая перемешивает (переупорядочивает случайным образом) элементы массива.

Многократные прогоны через shuffle могут привести к разным последовательностям элементов. Например:
 */

function shuffle(array) {
  arr.sort(Math.random * 2 - 1);
}

/*
Напишите функцию getAverageAge(users), которая принимает массив объектов со свойством age и возвращает средний возраст.

Формула вычисления среднего арифметического значения: (age1 + age2 + ... + ageN) / N.
 */

let vasya = { name: "Вася", age: 25 };
let petya = { name: "Петя", age: 30 };
let masha = { name: "Маша", age: 29 };

let arr = [vasya, petya, masha];

alert(getAverageAge(arr)); // (25 + 30 + 29) / 3 = 28

function getAverageAge(array) {
  let agesTotal = array.reduce(function (total, user) {
    return total + user.age;
  }, 0);
  return agesTotal / array.length;
}

/*
Пусть arr – массив строк.

Напишите функцию unique(arr), которая возвращает массив, содержащий только уникальные элементы arr.
 */

function unique(arr) {
  return Array.from(
    new Set(arr) //имхо самый очевидный, читаемый и оптимальный вариант - делать это через сет
  );
}

let strings = [
  "кришна",
  "кришна",
  "харе",
  "харе",
  "харе",
  "харе",
  "кришна",
  "кришна",
  ":-O",
];

alert(unique(strings)); // кришна, харе, :-O

// Вариант принциально без использования сетов

function unique(arr) {
  let outputArr = [];
  arr.forEach((item) => {
    if (!outputArr.includes(item)) outputArr.push(item);
  });
  return outputArr;
}

let strings = [
  "кришна",
  "кришна",
  "харе",
  "харе",
  "харе",
  "харе",
  "кришна",
  "кришна",
  ":-O",
];

alert(unique(strings));
