/*
Напишите функцию sumTo(n), которая вычисляет сумму чисел 1 + 2 + ... + n.

Например:

sumTo(1) = 1
sumTo(2) = 2 + 1 = 3
sumTo(3) = 3 + 2 + 1 = 6
sumTo(4) = 4 + 3 + 2 + 1 = 10
...
sumTo(100) = 100 + 99 + ... + 2 + 1 = 5050
Сделайте три варианта решения:

С использованием цикла.
Через рекурсию, т.к. sumTo(n) = n + sumTo(n-1) for n > 1.
С использованием формулы арифметической прогрессии.
Пример работы вашей функции:

function sumTo(n) { }

alert( sumTo(100) ); // 5050
P.S. Какой вариант решения самый быстрый? Самый медленный? Почему?

    P.P.S. Можно ли при помощи рекурсии посчитать sumTo(100000)? Ответ: Нет, потому что лопнет стэк
 */

//1, циклический

function sumTo(n) {
  let total = 0;
  for (let i = 0; i <= n; i++) {
    total += i;
  }
  return total;
}

alert(sumTo(100));

//2, рекурсивный подход

function sumTo(n) {
  return n === 0 ? n : n + sumTo(n - 1);
}

alert(sumTo(100));

//3, формула. Самый быстрый вариант, потому что  минимум операций

function sumTo(n) {
  return ((1 + n) / 2) * n;
}
alert(sumTo(100));

/*
Факториал натурального числа – это число, умноженное на "себя минус один", затем на "себя минус два", и так далее до 1. Факториал n обозначается как n!

Определение факториала можно записать как:

n! = n * (n - 1) * (n - 2) * ...*1
Примеры значений для разных n:

1! = 1
2! = 2 * 1 = 2
3! = 3 * 2 * 1 = 6
4! = 4 * 3 * 2 * 1 = 24
5! = 5 * 4 * 3 * 2 * 1 = 120
Задача – написать функцию factorial(n), которая возвращает n!, используя рекурсию.

alert( factorial(5) ); // 120
 */

function factorial(n) {
  return n === 1 ? 1 : n * factorial(n - 1);
}

alert(factorial(5)); // 120

/*
Последовательность чисел Фибоначчи определяется формулой Fn = Fn-1 + Fn-2. То есть, следующее число получается как сумма двух предыдущих.

Первые два числа равны 1, затем 2(1+1), затем 3(1+2), 5(2+3) и так далее: 1, 1, 2, 3, 5, 8, 13, 21....

Числа Фибоначчи тесно связаны с золотым сечением и множеством природных явлений вокруг нас.

Напишите функцию fib(n) которая возвращает n-е число Фибоначчи.

P.S. Все запуски функций из примера выше должны работать быстро. Вызов fib(77) должен занимать не более доли секунды.

 */

function fib(n) {
  let f_n = 2,
    f_n_minusOne = 1,
    f_n_minusTwo = 1;

  if (n === 1) return f_n_minusTwo;
  if (n === 2) return f_n_minusOne;
  if (n === 3) return f_n;

  for (let i = 4; i <= n; i++) {
    f_n_minusTwo = f_n_minusOne;
    f_n_minusOne = f_n;
    f_n = f_n_minusOne + f_n_minusTwo;
  }

  return f_n;
}

alert(fib(3)); // 2
alert(fib(7)); // 13
alert(fib(77)); // 5527939700884757

/*
Допустим, у нас есть односвязный список (как описано в главе Рекурсия и стек):

let list = {
  value: 1,
  next: {
    value: 2,
    next: {
      value: 3,
      next: {
        value: 4,
        next: null
      }
    }
  }
};
Напишите функцию printList(list), которая выводит элементы списка по одному.

Сделайте два варианта решения: используя цикл и через рекурсию.

Как лучше: с рекурсией или без? // Ответ - цикл, потому что не зависим от размера стека, хотя рекурсия очевиднее.
 */

//Рекурсивно

const list = {
  value: 1,
  next: {
    value: 2,
    next: {
      value: 3,
      next: {
        value: 4,
        next: null,
      },
    },
  },
};

printListRecursive(list);

function printListRecursive(list) {
  let array = [];

  recursive(list);

  return array;

  function recursive(list) {
    array.push(list.value);

    if (!(list.next === null)) {
      recursive(list.next);
    }
  }
}

// С помощью цикла

const list = {
  value: 1,
  next: {
    value: 2,
    next: {
      value: 3,
      next: {
        value: 4,
        next: null,
      },
    },
  },
};

function printListCycle(list) {
  let clone = list,
    output = [clone.value];

  do {
    clone = clone.next;
    output.push(clone.value);
  } while (clone.next);

  return output;
}

printListCycle(list);

/*
 Выведите односвязный список из предыдущего задания Вывод односвязного списка в обратном порядке.

 Сделайте два решения: с использованием цикла и через рекурсию.
 */

//Простой вариант - расширить функционал имеющейся функции

function printListReverseCycleExtended(list) {
  return printListCycle(list).reverse();
}

function printListReverseRecursiveExtended(list) {
  return printListRecursive(list).reverse();
}

//Специальная независимая рекурсивная функция

function printListReverseRecursive(list) {
  const output = [];

  recursiveStep(list);

  return output;

  function recursiveStep(list) {
    if (list.next) {
      recursiveStep(list.next);
    }
    output.push(list.value);
  }
}

// Независимая функция на основе цикла будет дублировать код имеющейся printListCycle(list),
// но в конце будет .reverse() выходной массив

function printListReverseCycle(list) {
  let clone = list,
    output = [clone.value];

  do {
    clone = clone.next;
    output.push(clone.value);
  } while (clone.next);

  return output.reverse();
}
