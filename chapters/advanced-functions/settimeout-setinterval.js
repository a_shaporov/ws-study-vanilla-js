/*
 Напишите функцию printNumbers(from, to), которая выводит число каждую секунду, начиная от from и заканчивая to.

 Сделайте два варианта решения.

 Используя setInterval.
 Используя рекурсивный setTimeout.
 */

function printNumbers(from, to) {
  let i = from;
  const id = setInterval(() => {
    if (i === to) {
      clearInterval(id);
    }
    alert(i);
    i++;
  }, 1000);
}

printNumbers(5, 10);

// Recursive

function printNumbers(from, to) {
  let i = from;
  let id = setTimeout(function counter() {
    console.log(i);
    i++;
    if (i <= to) {
      id = setTimeout(counter, 1000);
    }
  }, 1000);
}

printNumbers(5, 10);
